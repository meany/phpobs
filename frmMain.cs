﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace PHPObs {
    public partial class frmMain : Form {
        public frmMain() {
            InitializeComponent();
            PaintConnectionControls(false);
        }

        private void PaintConnectionControls(bool IsConnected) {
            cbxSelectAllTables.Checked = false;
            if (IsConnected) {
                btnConnect.BackColor = Color.Green;
                btnConnect.Text = "Disconnect";
                cbxDb.Enabled = false;
                txtServer.Enabled = false;
                txtDb.Enabled = false;
                txtUser.Enabled = false;
                txtPassword.Enabled = false;
                cbxSelectAllTables.Enabled = true;
                cbxTables.Enabled = true;
                cbxTables.Items.Clear();
                grpClass.Enabled = true;
                btnGenerate.Enabled = true;
            } else {
                btnConnect.BackColor = SystemColors.Control;
                btnConnect.Text = "Connect";
                cbxDb.Enabled = true;
                txtServer.Enabled = true;
                txtDb.Enabled = true;
                txtUser.Enabled = true;
                txtPassword.Enabled = true;
                cbxSelectAllTables.Enabled = false;
                cbxTables.Enabled = false;
                cbxTables.Items.Clear();
                grpClass.Enabled = false;
                btnGenerate.Enabled = false;
            }
        }

        private void btnConnect_Click(object sender, EventArgs e) {
            if (btnConnect.Text == "Connect") {
                if ((string)cbxDb.SelectedItem == "MySQL") {
                    string cs = string.Format("server={0};database={1};userid={2};password={3}", txtServer.Text, txtDb.Text, txtUser.Text, txtPassword.Text);
                    try {
                        using (MySqlConnection conn = new MySqlConnection()) {
                            conn.ConnectionString = cs;
                            conn.Open();
                            using (MySqlCommand cmd = new MySqlCommand()) {
                                cmd.Connection = conn;
                                cmd.CommandText = @"SELECT DISTINCT TABLE_NAME 
                                            FROM INFORMATION_SCHEMA.COLUMNS
                                            WHERE TABLE_SCHEMA = @schema";
                                cmd.Parameters.AddWithValue("@schema", txtDb.Text);
                                using (MySqlDataReader rdr = cmd.ExecuteReader()) {
                                    PaintConnectionControls(true);
                                    while (rdr.Read())
                                        cbxTables.Items.Add(rdr[0].ToString());
                                }
                            }
                        }
                    } catch (MySqlException ex) {
                        MessageBox.Show(string.Format("Error: {0}", ex.ToString()), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        PaintConnectionControls(false);
                    }
                }
            } else {
                PaintConnectionControls(false);
            }
        }

        private void cbxSelectAllTables_CheckedChanged(object sender, EventArgs e) {
            for (int i = 0; i < cbxTables.Items.Count; i++)
                cbxTables.SetItemChecked(i, cbxSelectAllTables.Checked);
        }

        private void btnGenerate_Click(object sender, EventArgs e) {
            foreach (object item in cbxTables.CheckedItems) {
                string table = item.ToString();
                string txtOut = @"<?php

class ";
                txtOut += (cbxClassSingular.Checked) ? ConvertToSingular(table) : table;
                txtOut += " " + txtExtends.Text + @" {

    // columns
";
                DataTable columns = GetColumns(table);
                txtOut += GenerateColumns(columns);

                if (cbxCtor.Checked) {
                    txtOut += @"
    function __construct() {
";
                    txtOut += GenerateCtor(columns);
                    txtOut += "    }";
                }

                if (cbxGet.Checked) {
                    txtOut += @"

    // static gets
    public static function Get($id) {
        $connection = _db::Connect();
        $query = 'SELECT * FROM `" + table + @"` WHERE `id`=""' . intval($id) . '"" LIMIT 1';
        return self::GetQuery($query, $connection, Array())[0];
    }
";
                }

                if (cbxGetItems.Checked) {
                    txtOut += @"
    public static function GetItems($fcv_array = array(), $sortBy = '', $ascending = true, $limit = '') {
        $connection = _db::Connect();
        $sqlLimit = ($limit != '' ? 'LIMIT ' . $limit : '');
        $query = 'SELECT * FROM `" + table + @"` ';
        if (sizeof($fcv_array) > 0) {
            $query .= ' WHERE ';
            for ($i = 0; $i < sizeof($fcv_array); $i++) {
                if (sizeof($fcv_array[$i]) === 1) {
                    $query .= ' ' . $fcv_array[$i][0] . ' ';
                    continue;
                } else {
                    if ($i > 0 && sizeof($fcv_array[$i - 1]) != 1) {
                        $query .= ' AND ';
                    }
                    $value = _dbBase::IsColumn($fcv_array[$i][2]) ? $fcv_array[$i][2] : '""' . $fcv_array[$i][2] . '""';
                    $query .= '`' . $fcv_array[$i][0] . '` ' . $fcv_array[$i][1] . ' ' . $value;
                }
            }
        }
        $sortBy = ($sortBy != '') ? $sortBy : 'id';
        $query .= ' ORDER BY `' . $sortBy . '` ' . ($ascending ? 'ASC ' : 'DESC ') . $sqlLimit;
        return self::GetQuery($query, $connection, Array());
    }
";
                }

                if (cbxGet.Checked || cbxGetItems.Checked) {
                    txtOut += @"
    private static function GetQuery($query, $connection, $items) {
        $cursor = _db::Reader($query, $connection);
        while ($row = _db::Read($cursor)) {
            $item = new static();
";
                    txtOut += GenerateItemsUnescaped(columns);
                    txtOut += @"            $items[] = $item;
        }
        return $items;
    }
";
                }

                if (cbxSave.Checked) {
                    txtOut += @"
    // insert/update/delete
    public function Save() {
        $connection = _db::Connect();
        $query = '';
        if ($this->id === 0) {
            $query = 'INSERT INTO `" + table + @"` (";
                    txtOut += GenerateSQLInsertItems(columns);
                    txtOut += @") VALUES (
";
                    txtOut += GenerateSQLInsertValues(columns);
                    txtOut += @"
            $this->id = _db::InsertOrUpdate($query, $connection);
        } else {
            $query = 'UPDATE `" + table + @"` SET
";
                    txtOut += GenerateSQLUpdate(columns);
                    txtOut += @"
            _db::InsertOrUpdate($query, $connection);
        }
        return $this->id;
    }
";
                }

                if (cbxSaveNew.Checked) {
                    txtOut += @"
    public function SaveNew() {
        $this->id = 0;
        return $this->Save();
    }
";
                }

                if (cbxDelete.Checked) {
                    txtOut += @"
    public function Delete() {
        $connection = _db::Connect();
        $query = 'DELETE FROM `" + table + @"` WHERE `id`=""' . $this->id . '""';
        return _db::NonQuery($query, $connection);
    }
";
                }

                txtOut += @"
}

?>";

                frmOutput frmOutput = new frmOutput();
                frmOutput.Show();
                frmOutput.TextOutput = txtOut;
                frmOutput.Caption = string.Format("Output - PHPObs ({0})", item);
            }
        }

        private DataTable GetColumns(string table) {
            DataTable dt = new DataTable();
            dt.Columns.Add("Field", typeof(string));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("Default", typeof(string));
            if ((string)cbxDb.SelectedItem == "MySQL") {
                string cs = string.Format("server={0};database={1};userid={2};password={3}", txtServer.Text, txtDb.Text, txtUser.Text, txtPassword.Text);
                try {
                    using (MySqlConnection conn = new MySqlConnection()) {
                        conn.ConnectionString = cs;
                        conn.Open();
                        using (MySqlCommand cmd = new MySqlCommand()) {
                            cmd.Connection = conn;
                            cmd.CommandText = @"SHOW COLUMNS FROM " + table;
                            using (MySqlDataReader rdr = cmd.ExecuteReader()) {
                                while (rdr.Read()) {
                                    DataRow dr = dt.NewRow();
                                    dr["Field"] = rdr["Field"].ToString();
                                    dr["Type"] = rdr["Type"].ToString();
                                    dr["Default"] = rdr["Default"].ToString();
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                    }
                } catch (MySqlException ex) {
                    MessageBox.Show(string.Format("Error: {0}", ex.ToString()), this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return dt;
        }

        private string GenerateColumns(DataTable columns) {
            string s = string.Empty;
            foreach (DataRow dr in columns.Rows)
                s += "    public $" + dr["Field"].ToString() + ";\r\n";
            return s;
        }

        private string GenerateCtor(DataTable columns) {
            string s = string.Empty;
            foreach (DataRow dr in columns.Rows) {
                string field = dr["Field"].ToString();
                string type = GetType(dr["Type"].ToString());
                string defaul = dr["Default"].ToString();

                s += "        $this->" + field + " = ";
                switch (type) {
                    case "int":
                        if (defaul != string.Empty)
                            s += defaul;
                        else
                            s += GetDefaultValue(type);
                        break;
                    case "timestamp":
                    case "datetime":
                        if (defaul == "CURRENT_TIMESTAMP")
                            s += "gmdate('Y-m-d H:i:s')";
                        else
                            s += GetDefaultValue(type);
                        break;
                    default:
                        if (defaul != string.Empty)
                            s += "'" + defaul + "'";
                        else
                            s += GetDefaultValue(type);
                        break;
                }
                s += ";\r\n";
            }
            return s;
        }

        private string GenerateItemsUnescaped(DataTable columns) {
            string s = string.Empty;
            foreach (DataRow dr in columns.Rows) {
                string field = dr["Field"].ToString();
                string type = GetType(dr["Type"].ToString());
                s += "            $item->" + field + " = ";
                switch (type) {
                    case "int":
                        s += "(int) $item->Unescape($row['" + field + "']);";
                        break;
                    case "timestamp":
                    case "datetime":
                        s += "strtotime($item->Unescape($row['" + field + "']) . ' UTC');";
                        break;
                    default:
                        s += "$item->Unescape($row['" + field + "']);";
                        break;
                }
                s += "\r\n";
            }
            return s;
        }

        private string GenerateSQLInsertItems(DataTable columns) {
            string s = string.Empty;
            RemoveRowByFieldName(columns, "id");
            int i = 1;
            foreach (DataRow dr in columns.Rows) {
                string field = dr["Field"].ToString();
                s += "`" + field + "`";
                s += (i++ == columns.Rows.Count) ? string.Empty : ", ";
            }
            return s;
        }

        private string GenerateSQLInsertValues(DataTable columns) {
            string s = string.Empty;
            RemoveRowByFieldName(columns, "id");
            int i = 1;
            foreach (DataRow dr in columns.Rows) {
                string field = dr["Field"].ToString();
                string type = GetType(dr["Type"].ToString());
                if (type == "int" || type == "tinyint") {
                    s += "                \"' . (int) $this->" + field + " . '\"";
                } else if (type == "datetime" || type == "timestamp") {
                    s += "                FROM_UNIXTIME(' . $this->" + field + " . ')";
                } else {
                    s += "                \"' . $this->Escape($this->" + field + ") . '\"";
                }
                s += (i++ == columns.Rows.Count) ? ")';" : ",\r\n";
            }
            return s;
        }

        private string GenerateSQLUpdate(DataTable columns) {
            string s = string.Empty;
            RemoveRowByFieldName(columns, "id");
            RemoveRowByFieldName(columns, "created");
            RemoveRowByFieldName(columns, "createdBy");
            int i = 1;
            foreach (DataRow dr in columns.Rows) {
                string field = dr["Field"].ToString();
                string type = GetType(dr["Type"].ToString());
                if (type == "int" || type == "tinyint") {
                    s += "                `" + field + "`=\"' . (int) $this->" + field + " . '\"";
                } else if (type == "datetime" || type == "timestamp") {
                    s += "                `" + field + "`=FROM_UNIXTIME(' . $this->" + field + " . ')";
                } else {
                    s += "                `" + field + "`=\"' . $this->Escape($this->" + field + ") . '\"";
                }
                s += (i++ == columns.Rows.Count) ? " WHERE `id`=\"' . $this->id . '\"';" : ",\r\n";
            }
            return s;
        }

        private string ConvertToSingular(string plural) {
            string lower = plural.ToLower();
            string result = plural;

            if (lower.EndsWith("ies") && "aeiou".IndexOf(lower.Substring(lower.Length - 4, 1)) < 0)
                result = plural.Substring(0, plural.Length - 3) + "y";
            else
                result = plural.Substring(0, plural.Length - 1);
            if (plural == lower)
                return result.ToLower();
            else if (plural == plural.ToUpper())
                return result.ToUpper();
            else
                return result;
        }

        private string GetType(string type) {
            return (type.IndexOf('(') > 0) ? type.Substring(0, type.IndexOf('(')) : type;
        }

        private string GetDefaultValue(string type) {
            string result = string.Empty;
            switch (type) {
                case "tinyint":
                    result = "false";
                    break;
                case "varchar":
                    result = "''";
                    break;
                case "timestamp":
                    result = "gmdate('Y-m-d H:i:s', 0)";
                    break;
                case "int":
                    result = "0";
                    break;
                default:
                    result = "<undefined>";
                    break;
            }
            return result;
        }

        public static void RemoveRowByFieldName(DataTable dt, string field) {
            for (int i = dt.Rows.Count - 1; i >= 0; i--)
                if (dt.Rows[i]["Field"].ToString() == field)
                    dt.Rows[i].Delete();
            dt.AcceptChanges();
        }

        private void txtUser_TextChanged(object sender, EventArgs e) {

        }
    }
}
