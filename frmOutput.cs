﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PHPObs {
    public partial class frmOutput : Form {
        public frmOutput() {
            InitializeComponent();
        }

        public string TextOutput {
            set {
                txtOutput.Text = value;
            }
        }
        public string Caption {
            set {
                this.Text = value;
            }
        }

        private void frmOutput_Load(object sender, EventArgs e) {

        }

        private void txtOutput_TextChanged(object sender, EventArgs e) {
            txtOutput.SelectAll();
        }

        private void txtOutput_Click(object sender, EventArgs e) {
            //txtOutput.SelectAll();
        }

    }
}
