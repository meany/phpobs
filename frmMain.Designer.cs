﻿namespace PHPObs {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnConnect = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDb = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.cbxDb = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbxSelectAllTables = new System.Windows.Forms.CheckBox();
            this.cbxTables = new System.Windows.Forms.CheckedListBox();
            this.grpClass = new System.Windows.Forms.GroupBox();
            this.cbxDelete = new System.Windows.Forms.CheckBox();
            this.cbxSaveNew = new System.Windows.Forms.CheckBox();
            this.cbxSave = new System.Windows.Forms.CheckBox();
            this.cbxGetItems = new System.Windows.Forms.CheckBox();
            this.cbxGet = new System.Windows.Forms.CheckBox();
            this.cbxCtor = new System.Windows.Forms.CheckBox();
            this.txtExtends = new System.Windows.Forms.TextBox();
            this.cbxClassSingular = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.grpClass.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.btnGenerate);
            this.groupBox1.Controls.Add(this.btnConnect);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtUser);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDb);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtServer);
            this.groupBox1.Controls.Add(this.cbxDb);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(215, 244);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Database";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(6, 187);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(199, 37);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.AutoSize = true;
            this.btnConnect.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnConnect.Location = new System.Drawing.Point(66, 156);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(57, 23);
            this.btnConnect.TabIndex = 10;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 132);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password:";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(66, 129);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(143, 21);
            this.txtPassword.TabIndex = 8;
            this.txtPassword.Text = "p4stA6asp5CaCRufrac$UgE5";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "UserID:";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(66, 102);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(143, 21);
            this.txtUser.TabIndex = 6;
            this.txtUser.Text = "desdoesvintage";
            this.txtUser.TextChanged += new System.EventHandler(this.txtUser_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Database:";
            // 
            // txtDb
            // 
            this.txtDb.Location = new System.Drawing.Point(66, 75);
            this.txtDb.Name = "txtDb";
            this.txtDb.Size = new System.Drawing.Size(143, 21);
            this.txtDb.TabIndex = 4;
            this.txtDb.Text = "desdoesvintage";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Server:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Driver:";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(66, 48);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(143, 21);
            this.txtServer.TabIndex = 1;
            this.txtServer.Text = "stang-fs";
            // 
            // cbxDb
            // 
            this.cbxDb.FormattingEnabled = true;
            this.cbxDb.Items.AddRange(new object[] {
            "MySQL"});
            this.cbxDb.Location = new System.Drawing.Point(66, 20);
            this.cbxDb.Name = "cbxDb";
            this.cbxDb.Size = new System.Drawing.Size(143, 21);
            this.cbxDb.TabIndex = 0;
            this.cbxDb.Text = "MySQL";
            // 
            // groupBox2
            // 
            this.groupBox2.AutoSize = true;
            this.groupBox2.Controls.Add(this.cbxSelectAllTables);
            this.groupBox2.Controls.Add(this.cbxTables);
            this.groupBox2.Location = new System.Drawing.Point(233, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(215, 244);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tables";
            // 
            // cbxSelectAllTables
            // 
            this.cbxSelectAllTables.AutoSize = true;
            this.cbxSelectAllTables.Enabled = false;
            this.cbxSelectAllTables.Location = new System.Drawing.Point(6, 21);
            this.cbxSelectAllTables.Name = "cbxSelectAllTables";
            this.cbxSelectAllTables.Size = new System.Drawing.Size(69, 17);
            this.cbxSelectAllTables.TabIndex = 1;
            this.cbxSelectAllTables.Text = "Select All";
            this.cbxSelectAllTables.UseVisualStyleBackColor = true;
            this.cbxSelectAllTables.CheckedChanged += new System.EventHandler(this.cbxSelectAllTables_CheckedChanged);
            // 
            // cbxTables
            // 
            this.cbxTables.CheckOnClick = true;
            this.cbxTables.FormattingEnabled = true;
            this.cbxTables.Location = new System.Drawing.Point(6, 44);
            this.cbxTables.Name = "cbxTables";
            this.cbxTables.Size = new System.Drawing.Size(203, 180);
            this.cbxTables.TabIndex = 0;
            // 
            // grpClass
            // 
            this.grpClass.AutoSize = true;
            this.grpClass.Controls.Add(this.cbxDelete);
            this.grpClass.Controls.Add(this.cbxSaveNew);
            this.grpClass.Controls.Add(this.cbxSave);
            this.grpClass.Controls.Add(this.cbxGetItems);
            this.grpClass.Controls.Add(this.cbxGet);
            this.grpClass.Controls.Add(this.cbxCtor);
            this.grpClass.Controls.Add(this.txtExtends);
            this.grpClass.Controls.Add(this.cbxClassSingular);
            this.grpClass.Location = new System.Drawing.Point(454, 12);
            this.grpClass.Name = "grpClass";
            this.grpClass.Size = new System.Drawing.Size(135, 245);
            this.grpClass.TabIndex = 2;
            this.grpClass.TabStop = false;
            this.grpClass.Text = "Class";
            // 
            // cbxDelete
            // 
            this.cbxDelete.AutoSize = true;
            this.cbxDelete.Checked = true;
            this.cbxDelete.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxDelete.Location = new System.Drawing.Point(6, 208);
            this.cbxDelete.Name = "cbxDelete";
            this.cbxDelete.Size = new System.Drawing.Size(87, 17);
            this.cbxDelete.TabIndex = 8;
            this.cbxDelete.Text = "public Delete";
            this.cbxDelete.UseVisualStyleBackColor = true;
            // 
            // cbxSaveNew
            // 
            this.cbxSaveNew.AutoSize = true;
            this.cbxSaveNew.Checked = true;
            this.cbxSaveNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxSaveNew.Location = new System.Drawing.Point(6, 185);
            this.cbxSaveNew.Name = "cbxSaveNew";
            this.cbxSaveNew.Size = new System.Drawing.Size(101, 17);
            this.cbxSaveNew.TabIndex = 7;
            this.cbxSaveNew.Text = "public SaveNew";
            this.cbxSaveNew.UseVisualStyleBackColor = true;
            // 
            // cbxSave
            // 
            this.cbxSave.AutoSize = true;
            this.cbxSave.Checked = true;
            this.cbxSave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxSave.Location = new System.Drawing.Point(6, 162);
            this.cbxSave.Name = "cbxSave";
            this.cbxSave.Size = new System.Drawing.Size(80, 17);
            this.cbxSave.TabIndex = 6;
            this.cbxSave.Text = "public Save";
            this.cbxSave.UseVisualStyleBackColor = true;
            // 
            // cbxGetItems
            // 
            this.cbxGetItems.AutoSize = true;
            this.cbxGetItems.Checked = true;
            this.cbxGetItems.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxGetItems.Location = new System.Drawing.Point(6, 116);
            this.cbxGetItems.Name = "cbxGetItems";
            this.cbxGetItems.Size = new System.Drawing.Size(108, 17);
            this.cbxGetItems.TabIndex = 4;
            this.cbxGetItems.Text = "public ::GetItems";
            this.cbxGetItems.UseVisualStyleBackColor = true;
            // 
            // cbxGet
            // 
            this.cbxGet.AutoSize = true;
            this.cbxGet.Checked = true;
            this.cbxGet.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxGet.Location = new System.Drawing.Point(7, 93);
            this.cbxGet.Name = "cbxGet";
            this.cbxGet.Size = new System.Drawing.Size(81, 17);
            this.cbxGet.TabIndex = 3;
            this.cbxGet.Text = "public ::Get";
            this.cbxGet.UseVisualStyleBackColor = true;
            // 
            // cbxCtor
            // 
            this.cbxCtor.AutoSize = true;
            this.cbxCtor.Checked = true;
            this.cbxCtor.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxCtor.Location = new System.Drawing.Point(7, 70);
            this.cbxCtor.Name = "cbxCtor";
            this.cbxCtor.Size = new System.Drawing.Size(83, 17);
            this.cbxCtor.TabIndex = 2;
            this.cbxCtor.Text = "__construct";
            this.cbxCtor.UseVisualStyleBackColor = true;
            // 
            // txtExtends
            // 
            this.txtExtends.Location = new System.Drawing.Point(7, 42);
            this.txtExtends.Name = "txtExtends";
            this.txtExtends.Size = new System.Drawing.Size(122, 21);
            this.txtExtends.TabIndex = 1;
            this.txtExtends.Text = "extends _dbBase";
            // 
            // cbxClassSingular
            // 
            this.cbxClassSingular.AutoSize = true;
            this.cbxClassSingular.Checked = true;
            this.cbxClassSingular.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbxClassSingular.Location = new System.Drawing.Point(7, 18);
            this.cbxClassSingular.Name = "cbxClassSingular";
            this.cbxClassSingular.Size = new System.Drawing.Size(122, 17);
            this.cbxClassSingular.TabIndex = 0;
            this.cbxClassSingular.Text = "Singular Class Name";
            this.cbxClassSingular.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 266);
            this.Controls.Add(this.grpClass);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PHPObs";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.grpClass.ResumeLayout(false);
            this.grpClass.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtServer;
        private System.Windows.Forms.ComboBox cbxDb;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbxSelectAllTables;
        private System.Windows.Forms.CheckedListBox cbxTables;
        private System.Windows.Forms.GroupBox grpClass;
        private System.Windows.Forms.TextBox txtExtends;
        private System.Windows.Forms.CheckBox cbxClassSingular;
        private System.Windows.Forms.CheckBox cbxDelete;
        private System.Windows.Forms.CheckBox cbxSaveNew;
        private System.Windows.Forms.CheckBox cbxSave;
        private System.Windows.Forms.CheckBox cbxGetItems;
        private System.Windows.Forms.CheckBox cbxGet;
        private System.Windows.Forms.CheckBox cbxCtor;
        private System.Windows.Forms.Button btnGenerate;
    }
}

